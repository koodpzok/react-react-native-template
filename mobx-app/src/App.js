import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { observer, inject } from 'mobx-react';
import { decorate, configure, action } from 'mobx';
// configure({ enforceActions: 'observed' });
class App extends Component {
  clickGOGO = action(() => {
    this.props.store.current = Date.now();
  });
  // clickGOGO = () => {
  //   this.props.store.current = Date.now();
  // };

  render() {
    console.log(this.props.store);
    return (
      <React.Fragment>
        <p>Current Time: {this.props.store.current}</p>
        <p>Elapsed Time: {this.props.store.elapsedTime}</p>
        <button className='btn btn-primary' onClick={this.clickGOGO}>
          PRESS
        </button>
      </React.Fragment>
    );
  }
}

// decorate(App, {
//   clickGOGO: action
// });

export default inject('store')(observer(App));
