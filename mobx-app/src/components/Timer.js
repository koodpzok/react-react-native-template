import {
  observable,
  computed,
  action,
  autorun,
  decorate,
  configure
} from 'mobx';
// configure({ enforceActions: 'observed' });
class Timer {
  //   @observable
  start = Date.now();
  //   @observable
  current = Date.now();

  a = [3, 29, 2];

  //   @computed
  get elapsedTime() {
    return this.current - this.start + 'milliseconds';
  }

  //   @action
  // tick() {
  //   this.current = Date.now();
  // }
}
decorate(Timer, {
  start: observable,
  current: observable,
  elapsedTime: computed
});

var store = (window.store = new Timer());
export default store;

// autorun(() => {
//   console.log(store.a[2].toString());
// });
