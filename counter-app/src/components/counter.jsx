import React, { Component } from 'react';

class Counter extends Component {
  state = {
    count: 0,
    imageUrl: 'https://picsum.photos/200',
    address: { name: 'bb' },
    tags: [
      { id: 1, name: 'tag1' },
      { id: 2, name: 'tag2' },
      { id: 3, name: 'tag3' }
    ]
  };

  // constructor() {
  //   super();
  //   this.handleIncrement = this.handleIncrement.bind(this); //so this function can reference to this by using bind
  // }

  renderTags() {
    if (this.state.tags.length === 0) return <p>There are no tags</p>;

    return (
      <ul>
        {this.state.tags.map(element => (
          <li key={element['id']}>
            id: {element['id']} name: {element['name']}
          </li>
        ))}
      </ul>
    );
  }

  handleIncrement = product => {
    console.log(product);
    console.log('Increment Clicked', this.state.count);
    this.setState({
      count: this.state.count + 1
    });
  };

  doHandleIncrement = () => {
    this.handleIncrement({ id: 12 });
  };

  render() {
    let classes = this.getBadgeClasses();
    // let a = this.statess.tags[0]['id'];
    // console.log(a);
    return (
      <React.Fragment>
        <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        <button
          onClick={this.doHandleIncrement}
          className='btn btn-primary btn-sm'
        >
          Increment
        </button>
      </React.Fragment>
    );
  }

  getBadgeClasses() {
    let classes = 'badge m-5 ';
    classes += this.state.count === 0 ? 'badge-warning ' : 'badge-primary';
    return classes;
  }

  formatCount() {
    // const { count } = this.statess;
    const count = this.state.count;
    // return count == 0 ? 'Zzzero' : 'Something else';
    return count === 0 ? <p>this is zeroo</p> : this.state.count;
  }
}

export default Counter;
