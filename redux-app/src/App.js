import React, { Component } from 'react';
import Counter from './components/Counter';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

// const initialState = {
//   count: 1
// };
// const initialState = {count: 1};
//reducer function stores state
function reducer(state, action) {
  //the state is firstly uninitialized
  if (state === undefined)
    state = {
      count: 1
    };

  if (action.type === 'DECREMENT') {
    //dont use state.count = state.count - 1;
    //becoz state is immutable

    return {
      count: state.count - 1
    };
  }
  if (action.type === 'INCREMENT') {
    return {
      count: state.count + 1
    };
  }

  return state; //return current state
}

const store = createStore(reducer); //so store contains state
// store.dispatch({ type: 'INCREMENT' });
// store.dispatch({ type: 'DECREMENT' });
// store.dispatch({ type: 'DECREMENT' });
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Counter />
        {this.props.rand}
      </Provider>
    );
  }
}

export default App;
export const Azz = 6;
