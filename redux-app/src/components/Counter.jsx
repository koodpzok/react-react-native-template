import React, { Component } from 'react';
import './Counter.css';
import { connect } from 'react-redux';

class Counter extends Component {
  incrementFunc = () => {
    this.props.dispatch({ type: 'INCREMENT' });
  };

  decrementFunc = () => {
    this.props.dispatch({ type: 'DECREMENT' });
  };

  testFunc = () => {
    this.props.dispatch({ type: 'TEST' });
  };

  render() {
    return (
      <div className='myDiv'>
        Counter
        <br />
        <button className='button-space' onClick={this.decrementFunc}>
          -
        </button>
        <span>{this.props.countz}</span>
        <button className='button-space' onClick={this.incrementFunc}>
          +
        </button>
        <button className='button-space' onClick={this.testFunc}>
          TEST
        </button>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  countz: state.count
});
export default connect(mapStateToProps)(Counter);
