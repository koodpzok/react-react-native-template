import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { observer, inject } from 'mobx-react';
import { decorate, configure, action } from 'mobx';
import Layout from './pages/Layout';
import myStore from './stores';
import * as API from './api/api';
// configure({ enforceActions: 'observed' });

// @inject("store")
// @observer

// const fetchGetCategory = () => {
//   return fetch('http://orderking-api.herokuapp.com/api/categories')
//     .then(response => response.json())
//     .then(responseJson => responseJson)
//     .catch(error => console.error(error));
// };

class App extends Component {
  // clickGOGO = action(() => {
  //   this.props.store.current = Date.now();
  // });
  // clickGOGO = () => {
  //   this.props.store.current = Date.now();
  // };
  lol = '';
  // async componentDidMount() {
  //   let a = await fetchGetCategory();
  //   console.log(a[1]['categoryName']);
  // }

  async componentWillMount() {
    let resultObj = await API.fetchGetCategory();
    resultObj.map(element => {
      this.props.store.addCategoryName(element['categoryName']);
    });
    // this.props.store.addCategoryNames('ss');
    console.log(this.props.store.categoryNames);
    console.log(resultObj);
  }
  // async fetchGetCategoryz() {
  //   let f = fetchGetCategory();
  //   await f;
  //   return f;
  // }
  render() {
    // console.log(this.fetchGetCategory);
    console.log(this.props.store.menu);

    // const categories = async function() {
    //   return await fetchGetCategory();
    // };
    // const categories = myStore.categoryConstructor().fetchGetCategory();
    // console.log(categories);
    return (
      <React.Fragment>
        <Layout>
          <p>
            MOKOMOKO<span>LPLPLPL </span>
          </p>
          <h1>KOKOKO </h1>
        </Layout>
        <p>Current Time: {this.props.store.current}</p>
        <p>Elapsed Time: {this.props.store.elapsedTime}</p>
        <p>name: {this.props.store.name}</p>
        <p>age: {this.props.store.age}</p>
        <p>grade: {this.props.store.grade}</p>
        <button
          className='btn btn-primary'
          onClick={this.props.store.clickSOSO}
        >
          PRESS
        </button>
        <button
          className='btn btn-primary'
          onClick={() => this.props.store.addaddMenu()}
        >
          adding!!
        </button>
        <button
          className='btn btn-danger'
          onClick={() => this.props.store.addNewCategory('Yggdrasil')}
        >
          adding!!
        </button>
        <ul>
          {this.props.store.menu.map(element => (
            <li key={element}>
              This is: {element['name']} {element['categories']}
            </li>
          ))}
        </ul>

        <ul>
          {this.props.store.categoryNames.map(element => (
            <li key={element}>this is type: {element}</li>
          ))}
        </ul>
      </React.Fragment>
    );
  }
}

// decorate(App, {
//   clickGOGO: action
// });

// export default App;
export default inject('store')(observer(App));
