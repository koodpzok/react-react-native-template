function fetchGetCategory() {
  return fetch('http://orderking-api.herokuapp.com/api/categories')
    .then(response => response.json())
    .then(responseJson => responseJson)
    .catch(error => console.error(error));
}
function hello() {
  console.log('111');
}
function fetchPostCategory(name) {
  return fetch('http://orderking-api.herokuapp.com/api/categories', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      categoryName: name
    })
  });
}
// export default API;
export { fetchGetCategory, fetchPostCategory };
// export { fetchGetCategory };
