import {
  observable,
  computed,
  action,
  autorun,
  decorate,
  configure
} from 'mobx';
import { observer, inject } from 'mobx-react';
import * as API from '../api/api';

class MenuItemStore {
  constructor(menuItem) {
    const { categories, name, price, preparingTime, description } = menuItem;
    this.categories = categories;
    this.description = description;
    this.name = name;
    this.price = price;
    this.preparingTime = preparingTime;
  }

  //   @observable
  categories = [];
  //   @observable
  name = '';
  //   @observable
  price = 0;
  //   @observable
  preparingTime = new Date();
  //   @observable
  description = '';

  addCategory(category) {
    if (!category) return;
    this.categories.push(new CategoryStore(category));
  }
}

decorate(MenuItemStore, {
  categories: observable,
  name: observable,
  price: observable,
  preparingTime: observable,
  description: observable
});

class CategoryStore {
  //   @observable
  // async fetchGetCategory() {
  //   let f = fetchGetCategory();
  //   await f;
  //   return f;
  // }
  //   set name(name) {
  //     if (!name) return;
  //     this.name = name;
  //   }
}
// decorate(CategoryStore, {
//   categoryNames: observable,
//   addCategoryNames: action
// });

class MenuStore {
  menu = ['33'];
  categoryNames = [];
  addCategoryName(name) {
    this.categoryNames.push(name);
  }

  addMenu(menuItem) {
    // null , undefined return out function
    // if (!menu) return; // If not have categ return out of function
    this.menu.push(new MenuItemStore(menuItem));
  }
  addaddMenu() {
    this.menu.push(
      new MenuItemStore({
        categories: 'Noodles',
        description: 'LADSFOASFODASF',
        name: 'EIEI',
        price: 4,
        preparingTime: new Date()
      })
    );
  }

  addNewCategory = categoryName => {
    if (!categoryName) return;

    //if fetchpost status == 200 do this
    API.fetchPostCategory(categoryName).then(response => {
      if (response.status === 200) this.addCategoryName(categoryName);
    });
  };
}
decorate(MenuStore, {
  menu: observable,
  addMenu: action,
  addaddMenu: action,
  categoryNames: observable,
  addCategoryNames: action,
  addNewCategory: action
});

var menuStore = new MenuStore();
export default menuStore;
// export default inject('store')(observer(CategoryStore));
// export default CategoryStore;
