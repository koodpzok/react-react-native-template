import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import * as Test from './Test';
// import MyInfo from './MyInfo';
import * as serviceWorker from './serviceWorker';
import Header from './Header';
import Body from './Body';
import Footer from './Footer';

function myJSX() {
  return (
    <ul>
      <li>1</li>
      <li>2</li>
      <li>3</li>
    </ul>
  );
}
function combineApp() {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

ReactDOM.render(combineApp(), document.getElementById('root'));
// ReactDOM.render(<MyInfo />, document.getElementById('root'));
