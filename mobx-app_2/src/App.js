import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { observer, inject } from 'mobx-react';
import { decorate, configure, action } from 'mobx';
import Layout from './pages/Layout';

// configure({ enforceActions: 'observed' });

// @inject("store")
// @observer
class App extends Component {
  // clickGOGO = action(() => {
  //   this.props.store.current = Date.now();
  // });
  // clickGOGO = () => {
  //   this.props.store.current = Date.now();
  // };

  render() {
    console.log(this.props.store);
    return (
      <React.Fragment>
        <Layout>
          <p>
            MOKOMOKO<span>LPLPLPL</span>
          </p>
          <h1>KOKOKO </h1>
        </Layout>
        <p>Current Time: {this.props.store.current}</p>
        <p>Elapsed Time: {this.props.store.elapsedTime}</p>
        <p>name: {this.props.store.name}</p>
        <p>age: {this.props.store.age}</p>
        <p>grade: {this.props.store.grade}</p>
        <button
          className='btn btn-primary'
          onClick={this.props.store.clickSOSO}
        >
          PRESS
        </button>
      </React.Fragment>
    );
  }
}

// decorate(App, {
//   clickGOGO: action
// });

// export default App;
export default inject('store')(observer(App));
