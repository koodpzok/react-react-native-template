import {
  observable,
  computed,
  action,
  autorun,
  decorate,
  configure
} from 'mobx';
// configure({ enforceActions: 'observed' });
class Timer {
  name = '';
  age = 0;
  grade = 0;
  // constructor() {}
  constructor(obj) {
    const { name, age, grade } = obj;
    this.name = name;
    this.age = age;
    this.grade = grade;
  }
  //  @observable
  start = Date.now();
  //   @observable
  current = Date.now();

  a = [3, 29, 2];

  //   @computed
  get elapsedTime() {
    return this.current - this.start + 'milliseconds';
  }

  clickSOSO() {
    this.current = Date.now();
  }
  //   @action
  // tick() {
  //   this.current = Date.now();
  // }
}

decorate(Timer, {
  start: observable,
  current: observable,
  elapsedTime: computed,
  clickSOSO: action.bound
});

// var store = (window.store = new Timer());
// export default store;
export default Timer;
// autorun(() => {
//   console.log(store.a[2].toString());
// });
