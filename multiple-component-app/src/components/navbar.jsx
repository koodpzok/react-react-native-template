import React, { Component } from 'react';
//Stateless functional component

//we must add props in functional component
const NavBar = props => {
  return (
    <nav className='navbar navbar-light bg-light'>
      <a className='navbar-brand' href='#'>
        Navbar
        <span className='badge badge-secondary m-2'>{props.noOfCounters}</span>
      </a>
    </nav>
  );
};

// class NavBar extends Component {
//   render() {
// return (
//     <nav className='navbar navbar-light bg-light'>
//       <a className='navbar-brand' href='#'>
//         Navbar
//         <span className='badge badge-secondary m-2'>
//           {this.props.noOfCounters}
//         </span>
//       </a>
//     </nav>
//   );
//   }
// }

export default NavBar;
