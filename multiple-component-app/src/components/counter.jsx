import React, { Component } from 'react';

class Counter extends Component {
  // state = {
  //   // count: 0
  //   count: this.props.value
  // };

  styles = {
    color: 'yellow',
    border: '1px solid black',
    padding: '1px',
    width: '150px'
  };

  // constructor(props) {
  //   super(props);
  //   console.log(this.props + ' zz');
  // }
  // incrementClick(props) {
  //   console.log(props + '  hello');
  //   // this.props.onIncrement(this.props.id);
  //   // this.setState({
  //   //   count: this.state.count + 1
  //   // });
  // }

  //
  incrementClick = () => {
    this.props.onIncrement(this.props.id);
  };

  deleteClick = () => {
    this.props.onDelete(this.props.id);
  };

  text = () => {
    if (this.props.value === 0) return <p>ZERO</p>;
    else return this.props.value;
  };

  render() {
    console.log('propsss from parent counters', this.props);

    return (
      <div>
        <span className='badge badge-primary m-2' style={this.styles}>
          {this.text()}
        </span>
        <button
          onClick={this.incrementClick}
          className='btn btn-secondary btn-sm m-2'
        >
          Increment
        </button>
        <button
          onClick={this.deleteClick}
          className='btn btn-danger btn-sm m-2'
        >
          Delete
        </button>
      </div>
    );
  }
}

export default Counter;
