import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
  render() {
    // onClick={this.handleReset}
    return (
      <React.Fragment>
        <h1>HELLO</h1>
        <button
          onClick={this.props.onReset}
          className='btn btn-primary btn-sm m-2'
        >
          Reset
        </button>
        {this.props.counters.map(element => (
          <Counter
            key={element['id']}
            value={element['value']}
            id={element['id']}
            onDelete={this.props.onDelete}
            onIncrement={this.props.onIncrement}
          >
            <h1>Counter #{element['id']}</h1>
          </Counter>
        ))}
      </React.Fragment>
    );
  }
}

export default Counters;
