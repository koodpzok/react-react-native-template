import React, { Component } from 'react';
import NavBar from './components/navbar';
import Counters from './components/counters';
// import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 11 },
      { id: 2, value: 2 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };

  handleIncrement = counterId => {
    const counters = this.state.counters.map(element => {
      if (element.id === counterId) {
        element.value = element.value + 1;
        return element;
      }
      return element;
    });
    this.setState({
      counters: counters
    });
  };

  handleReset = () => {
    const counters = this.state.counters.map(element => {
      element.value = 0;
      return element;
    });

    this.setState({
      counters: counters
    });
  };

  handleDelete = counterId => {
    const counters = this.state.counters.filter(c => c.id !== counterId); //anything that wont be deleted will be here
    this.setState({
      counters: counters
    });
    console.log('Event handler called', counterId);
  };

  render() {
    return (
      <React.Fragment>
        <NavBar
          noOfCounters={
            this.state.counters.filter(element => element.value > 0).length
          }
        />
        <main className='container'>
          <Counters
            onDelete={this.handleDelete}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            counters={this.state.counters}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
